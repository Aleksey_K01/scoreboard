package com.example.score;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer counter = 0;
    private Integer counter1 = 0;
    private final String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate");
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("counter", counter);
        outState.putInt("counter1", counter1);
        Log.d(TAG, "onSaveInstanceState");
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("counter")) {
            counter = savedInstanceState.getInt("counter");
            counter1 = savedInstanceState.getInt("counter1");
        }
        Log.d(TAG, "onRestoreInstanceState");
    }


    public void onClickBtnStudents(View view) {
        counter++;
        TextView counterViev = findViewById(R.id.numberOfStudents);
        counterViev.setText(counter.toString());
    }

    public void onClickBtnStudents1(View view) {
        counter1++;
        TextView counterViev = (TextView) findViewById(R.id.numberOfStudents1);
        counterViev.setText(counter1.toString());
    }

    public void onClickBtnReset(View view) {
        counter = 0;
        counter1 = 0;
        TextView counterView = (TextView) findViewById(R.id.numberOfStudents);
        TextView counterView1 = (TextView) findViewById(R.id.numberOfStudents1);
        counterView.setText(counter.toString());
        counterView1.setText(counter1.toString());
    }
}